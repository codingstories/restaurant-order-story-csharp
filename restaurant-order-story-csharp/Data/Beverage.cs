﻿namespace RestaurantOrderStory.Data
{
    public class Beverage : OrderItem
    {
        public BeverageType Type { get; set; }

        public BeverageSize Size { get; set; }

        public enum BeverageType
        {
            Alcoholic,
            NonAlcoholic
        }

        public enum BeverageSize
        {
            Small,
            Normal,
            Large
        }

        public override void SetPriority()
        {
            switch (Size)
            {
                case BeverageSize.Small:
                    Priority = Type == BeverageType.Alcoholic ? 3 : 1;
                    break;

                case BeverageSize.Normal:
                    Priority = 2;
                    break;

                case BeverageSize.Large:
                    Priority = Type == BeverageType.Alcoholic ? 1 : 3;
                    break;
            }
        }
    }
}
